<?php
$this->pageTitle = Yii::app()->name . ' - ' . Yii::app()->name;

$this->breadcrumbs = array(
    'Кто здесь?',
);
?>
<h1>Кто здесь?</h1>

<p class="par">
    Я Влад. Письма можно слать на <a href="mailto:vlk.abakan@gmail.com">vlk.abakan@gmail.com</a>, скайп — <a href="skype:vlad-abakan">vlad-abakan</a>.<br>
    Пишите, не стесняйтесь.
</p>

<p class="par">
    В интернете встречаюсь на <a href="http://habrahabr.ru/users/werdender/">хабре</a>, <a href="http://stackoverflow.com/users/2004123/vlad">SO</a>, <a href="http://d3.ru/user/werdender">d3</a>, <a href="http://leprosorium.ru/users/werdender">лепре</a>, в <a href="http://werdender.livejournal.com/">ЖЖ</a>.
</p>

<p class="par">
    Спасибо за внимание. Приходите еще.
</p>