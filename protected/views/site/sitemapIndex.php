<?= '<?xml version = "1.0" encoding = "UTF-8" ?>' ?>
<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">

    <?php foreach ($model as $tag): ?>
        <sitemap>
            <loc><?= Yii::app()->params->host . '/sitemap/' . $tag->id ?></loc>
            <lastmod><?= date(DATE_W3C, $tag->lastUpdateTime) ?></lastmod>
        </sitemap>
    <?php endforeach; ?>
</sitemapindex>
