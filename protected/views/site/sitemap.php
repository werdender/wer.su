<?= '<?xml version="1.0" encoding="UTF-8"?>' ?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    <?php foreach ($model->posts as $post): ?>
        <url>
            <loc><?= Yii::app()->params->host . $post->url ?></loc>
            <lastmod><?= date(DATE_W3C, $post->updated) ?></lastmod>
        </url>
    <?php endforeach; ?>
</urlset>
