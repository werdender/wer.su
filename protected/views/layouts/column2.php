<?php $this->beginContent('//layouts/main'); ?>
<div class="span-80pr">
	<div id="content">
		<?php echo $content; ?>
	</div><!-- content -->
</div>
<div class="content-right">
	<div id="sidebar">
	<?php if(!Yii::app()->user->isGuest) $this->widget('blog.widgets.UserMenu'); ?>
	</div><!-- sidebar -->
</div>
<?php $this->endContent(); ?>