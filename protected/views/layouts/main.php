<!DOCTYPE html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="language" content="ru" />

    <?php
        $cs = Yii::app()->clientScript;
        $cs->registerCssFile('/css/ie.css', 'lt IE 8');
        $cs->registerCssFile('/css/form.css');
        $cs->registerCssFile('/css/screen.css');
        $cs->registerCssFile('/css/main.css');
        $cs->registerCssFile('/js/plugins/jquery.mainmenu.css');

        $cs->registerScriptFile('/js/jquery-1.10.2.min.js', CClientScript::POS_HEAD);
        $cs->registerScriptFile('/js/plugins/jquery.mainmenu.js', CClientScript::POS_HEAD);
        $cs->registerScriptFile('/js/app.js', CClientScript::POS_HEAD);
    ?>

    <title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body>
    <div id="header">
        <div id="logo"><?php echo CHtml::encode($this->pageHello); ?></div>
    </div><!-- header -->

    <div id="mainmenu">

        <?php
        $this->widget('zii.widgets.CMenu', array(
            'items' => array(
                array('label' => 'Рубрикатор', 'url' => array('/tags')),
                array('label' => 'Мне, кстати, можно написать', 'url' => array('/site/page', 'view' => 'about')),
                array('label' => 'Выйти', 'url' => array('/site/logout'), 'visible' => !Yii::app()->user->isGuest)
            ),
        ));
        ?>

        <?php if (isset($this->breadcrumbs)): ?>
            <div id="breadcrumbs">
                <?php
                $this->widget('zii.widgets.CBreadcrumbs', array(
                    'links' => $this->breadcrumbs,
                    'separator' => ' | '
                ));
                ?>
            </div>
        <?php endif ?>

    </div>

    <div class="container" id="page">


        <?php echo $content; ?>
        <div id="footerempty"></div>

    </div>


    <div id="footer">
        &copy; <?php echo date('Y'); ?> werdender<br/>
        Копирование материалов допускается при указании активной ссылки на источник.<br/>
        <div class="block right">
            <!-- Yandex.Metrika informer -->
            <a href="http://metrika.yandex.ru/stat/?id=22120408&amp;from=informer"
               target="_blank" rel="nofollow"><img src="//bs.yandex.ru/informer/22120408/3_0_FDFDF1FF_DDDDD1FF_0_pageviews"
                                                style="width:88px; height:31px; border:0;" alt="Яндекс.Метрика" title="Яндекс.Метрика: данные за сегодня (просмотры, визиты и уникальные посетители)" onclick="try{Ya.Metrika.informer({i:this,id:22120408,lang:'ru'});return false}catch(e){}"/></a>
            <!-- /Yandex.Metrika informer -->
        </div>

    </div>
    <noscript><div><img src="//mc.yandex.ru/watch/22120408" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
</body>
</html>
