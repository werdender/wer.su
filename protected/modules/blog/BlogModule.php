<?php

class BlogModule extends CWebModule
{
        public $postPerPage = 10;

        public function init()
	{
		$this->setImport(array(
			'blog.models.*',
			'blog.components.*',
                        'blog.widgets.*',
		));
	}

	public function beforeControllerAction($controller, $action) {

		if(parent::beforeControllerAction($controller, $action)) {
			return true;
		}
		else return false;
	}
}
