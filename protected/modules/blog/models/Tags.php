<?php

/**
 * This is the model class for table "{{blog_tags}}".
 *
 * The followings are the available columns in table '{{blog_tags}}':
 * @property string $id
 * @property string $tag
 *
 * The followings are the available model relations:
 * @property PostsTags[] $postsTags
 */
class Tags extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Tags the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{blog_tags}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('tag, alias', 'required'),
			array('tag, alias', 'length', 'max'=>255),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, tag', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'posts' => array(self::MANY_MANY,'Post',"{{blog_posts_tags}}(post_id,tag_id)"),
                        'lastUpdateTime' => array(self::STAT,'Post',"{{blog_posts_tags}}(post_id,tag_id)", 'select' => 'MAX(updated)'),
                        //'postsCount' => array(self::STAT,'Post',"{{blog_posts_tags}}(post_id,tag_id)"),
                        //'viewsCount' => array(self::STAT,'Post',"{{blog_posts_tags}}(post_id,tag_id)", 'select' => 'distinct sum(views_count)'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'tag' => 'Tag',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('tag',$this->tag,true);
                $criteria->compare('alias',$this->alias,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

        /*public function getCommentablePost() {
            if($this->postsCount) {
                $criteria=new CDbCriteria;
                $criteria->select = '*, id as ppost_id';
                $criteria->condition = 'id in ( select post_id from {{blog_posts_tags}} where tag_id ='.$this->id.')';
                $criteria->order='(select count(*) from {{comments}} where owner_id = ppost_id) desc';
                $criteria->limit = 1;
                return Post::model()->find($criteria);
            }
            return NULL;
        }*/

        public function getPopularPost() {
            if($this->postsCount) {
                $criteria=new CDbCriteria;
                $criteria->condition = 'id in ( select post_id from {{blog_posts_tags}} where tag_id ='.$this->id.')';
                $criteria->order='views_count desc';
                $criteria->limit = 1;
                return Post::model()->find($criteria);
            }
            return NULL;
        }

        public function getUrl()
        {
            return Yii::app()->createUrl('blog/tags/view',
                array(
                    'tag' => $this->alias,
                )
            );
        }

        public function getPostsCount() {
            return count($this->posts);
        }

        public function getViewsCount() {
            $count = 0;
            foreach($this->posts as $post) {
                $count += $post->views_count;
            };
            return $count;
        }

}