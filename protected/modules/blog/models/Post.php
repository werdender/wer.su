<?php

/**
 * This is the model class for table "{{blog_posts}}".
 *
 * The followings are the available columns in table '{{blog_posts}}':
 * @property string $id
 * @property string $theme
 * @property string $content
 * @property string $status
 * @property string $created
 * @property string $user_id
 * @property integer $comment_status
 * @property string $views_count
 * @property string $good_votes
 * @property string $bad_votes
 * @property integer $is_top
 *
 */
class Post extends CActiveRecord
{
        const STATUS_PUBLISHED = 1; // Опубликовано
        const STATUS_DRAFT     = 2; // Черновик
        const STATUS_ARCHIVED  = 3; // Архив

        public $highlighter = false;

        	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Post the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{blog_posts}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('theme, content', 'required'),
			array('comment_status, is_top', 'numerical', 'integerOnly'=>true),
			array('theme', 'length', 'max'=>255),
			array('post_status', 'length', 'max'=>1),
			array('created', 'length', 'max'=>11),
			array('user_id, views_count, good_votes, bad_votes', 'length', 'max'=>20),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, theme, content, post_status, created, user_id, comment_status, views_count, good_votes, bad_votes, is_top', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
                Yii::import('application.models.*');
		return array(
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
                        'tags' => array(self::MANY_MANY,'Tags',"{{blog_posts_tags}}(tag_id,post_id)"),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'theme' => 'Заголовок',
			'content' => 'Текст записи',
			'post_status' => 'Статус записи',
			'created' => 'Создана',
			'user_id' => 'Автор',
			'comment_status' => 'Комментарии разрешены',
			'views_count' => 'Количество просмотров',
			'good_votes' => 'Понравилось',
			'bad_votes' => 'Не понравилось',
			'is_top' => 'Всегда сверху',
                        'tags' => 'Теги'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('theme',$this->theme,true);
		$criteria->compare('content',$this->content,true);
		$criteria->compare('post_status',$this->post_status,true);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('user_id',$this->user_id,true);
		$criteria->compare('comment_status',$this->comment_status);
		$criteria->compare('views_count',$this->views_count,true);
		$criteria->compare('good_votes',$this->good_votes,true);
		$criteria->compare('bad_votes',$this->bad_votes,true);
		$criteria->compare('is_top',$this->is_top);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

        public function behaviors(){
             return array('CSaveRelationsBehavior' => array('class' => 'application.components.CSaveRelationsBehavior'));
        }

        protected function beforeSave()
        {
            if(parent::beforeSave())
            {
                if($this->isNewRecord)
                {
                    $this->created=time();
                    $this->user_id=Yii::app()->user->id;
                }
                $this->updated=time();
                //$this->content = str_replace ('<cut />', '<cut /><a name="cut"></a>', $this->content);
                return true;
            }
            else
                return false;
        }

        public function scopes()
        {
            return array(
                'published'=>array(
                    'condition'=>'post_status='.Post::STATUS_PUBLISHED,
                ),
                'recently'=>array(
                    'order'=>'t.is_top DESC, t.created DESC',
                ),
            );
        }

        public function defaultScope()
        {
            $condition = array(
                'condition' => 'post_status='.Post::STATUS_PUBLISHED,
                'order'=>'is_top DESC, created DESC'
            );
            if( ! Yii::app()->user->isGuest) {
                $condition['condition'] .= ' OR (post_status='.Post::STATUS_DRAFT.' AND user_id='.Yii::app()->user->id.')';
            }
            if(Yii::app()->request->getParam('id')) {
                $condition['condition'] .= ' OR (post_status='.Post::STATUS_ARCHIVED.')';
            }
            return $condition;
        }

        public function __toString()
        {
            return $this->getContent();
        }

        public function all()
        {
           return $this;
        }

        public function getHighlighter() {
            if( ! $this->highlighter) {
                $this->highlighter = new CTextHighlighter;
            }
            return $this->highlighter;
        }

        public function highlightCode($matches) {
            if(isset($matches[2])) {
                $h = $this->getHighlighter();
                if(isset($matches[1]) && $matches[1] !== '<code>' && $matches[1] !== '``') {
                    $h->language = $matches[1];
                }else{
                    $h->language = 'javascript';
                }
                if($matches[1] !== '``') {
                    return '<div class="code par">' . $h->highlight($matches[2]) . '</div>';
                }else{
                    return '<span class="inline">' . $h->highlight($matches[2]) . '</span>';
                }
            }
            return '';
        }

        public function delBr($matches) {
            if(isset($matches[1])) {
                return '<pre>'.str_replace('<br />', '', $matches[1]).'</pre>';
            }
        }

        public function getContent() {
            $content = preg_replace('#<jsf src="(.*)" tabs="(.*)" styles="(.*)"><\/jsf>#', '<iframe class="jsfiddle-embedded"  style="$3"  src="$1embedded/$2" allowfullscreen="allowfullscreen" frameborder="0"></iframe>', $this->content);

            $content = preg_replace_callback('#<code lang="(.*)">(.*)<\/code>#imsU', array($this, 'highlightCode'), $content);// function($matches) {
            $content = preg_replace_callback('#(<code>)(.*)<\/code>#imsU', array($this, 'highlightCode'), $content);// function($matches) {
            $content = preg_replace_callback('#(``)(.*)``#imsU', array($this, 'highlightCode'), $content);// function($matches) {


            return preg_replace_callback('#<pre>(.*)<\/pre>#imsU', array($this, 'delBr'), nl2br($content));
        }

        public function getUrl()
        {
            return Yii::app()->createUrl('blog/post/view',
                array(
                    'id' => $this->id,
                    'theme' => Text::translitURL($this->theme),
                )
            );
        }

        public function getDisqusIdentifier()
        {
            return 'post_'.$this->id;
        }


        public function getPostDate() {
            return date('d.m.Y г., в h:i', $this->created);
        }

        public function getPostRating() {
            return $this->good_votes - $this->bad_votes;
        }

        public function listStatus() {
            return array(
                Post::STATUS_PUBLISHED => 'Опубликовано',
                Post::STATUS_DRAFT => 'Черновик',
                Post::STATUS_ARCHIVED => 'Архив',
            );
        }

        public function listTags() {
            return CHtml::listData(Tags::model()->findAll(), 'id', 'tag');
        }

        public function getCutted() {
            $pos = strpos($this->getContent(), '<cut />');
            if($pos === FALSE) {
                return $this->getContent();
            }
            $cutted = substr($this->getContent(), 0, strpos($this->getContent(), '<cut />'));
            $cutted .= Yii::app()->controller->renderPartial('blog.views.post._cut', array('data'=>$this), true);
            return $cutted;
        }


}