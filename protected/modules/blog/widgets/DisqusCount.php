<?php

class DisqusCount extends CWidget {

    public function init() {
        parent::init();
    }

    public function run() {
        $this->render('DisqusCount');
    }
}