<?php

Yii::import('zii.widgets.jui.CJuiWidget');

class CommentsWidget extends CJuiWidget
{

    public $post;

    public function init()
    {
        parent::init();
    }

    public function run()
    {
        $this->render('CommentsWidget', array());
    }

}

?>
