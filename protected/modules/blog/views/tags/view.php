<?php
//var_dump($dataProvider);
$this->breadcrumbs=array(
	'Теги'=>'/tags',
	$model->tag,
);
?>

<?= Yii::app()->controller->renderPartial('_view', array('data'=>$model), true) ?>

<!---<div class="icon-rss right"><a href="#">RSS</a></div>-->

<?php
    $this->widget('BlogListView', array(
            'dataProvider'=> $dataProvider,
            'itemView'=>'blog.views.post._view',
            'template'=>"{items}\n{pager}",
            'ajaxUpdate' =>false,
            'pager'=>array(
                'cssFile'=>false,
                'header'=>false,
                'prevPageLabel'=>'←',
                'nextPageLabel'=>'→',
            ),
    ));

    $this->widget('DisqusCount');
?>
