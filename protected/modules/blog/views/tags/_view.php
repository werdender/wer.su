<?php if($data->postsCount): ?>
    <div class="tag-widget">
        <h1 class="float-left sub-main"><a class="icon-tag-big" href="<?=$data->url?>"><?=$data->tag?></a></h1>
        <div class="info">
            <div>всего статей: <?= $data->postsCount ?></div>
            <div>просмотров: <?= $data->viewsCount ?></div>
        </div>
        <div class="clear"></div>

        <div>Самая популярная статья: <a href="<?= $data->popularPost->url ?>"><?= $data->popularPost->theme ?></a></div>

    </div>
<?php endif; ?>
