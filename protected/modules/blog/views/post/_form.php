<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'post-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'theme'); ?>
		<?php echo $form->textField($model,'theme',array('size'=>114,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'theme'); ?>
	</div>

	<div class="row">
            <?php echo $form->labelEx($model,'content'); ?>
            <?php $this->widget('ext.markitup.EMarkitupWidget', array(
                'model' => $model,
                'attribute' => 'content',
                'options' => array(
                    'resizeHandle' => false,
                    'previewInWindow' => 'width=800, height=600, resizable=yes, scrollbars=yes',
                    'markupSet' => array(
                        array(
                            'name' => 'Жирный',
                             'openWith' => '<b>',
                             'closeWith' => '</b>',
                        ),
                        array(
                            'name' => 'Наклонный',
                             'openWith' => '<em>',
                             'closeWith' => '</em>',
                        ),
                        array(
                            'name' => 'Зачеркнутый',
                             'openWith' => '<del>',
                             'closeWith' => '</del>',
                        ),
                        array(
                            'separator' => '----------------'
                        ),
                        array(
                            'name' => 'Цитата',
                            'openWith' => '<blockquote>',
                            'closeWith' => '</blockquote>',
                        ),
                        array(
                            'name' => 'Сарказм',
                             'openWith' => '<p class="sarcasm">',
                             'closeWith' => '</p>',
                        ),
                        array(
                            'separator' => '----------------'
                        ),
                        array(
                            'name' => 'Ссылка',
                             'openWith' => '<a href="[![Ссылка:!:http://]!]">',
                             'closeWith' => '[![Текст]!]</a>',
                        ),
                        array(
                            'separator' => '----------------'
                        ),
                        array(
                            'name' => 'Код',
                            //'openWith' => '<jsf src="[![JSFiddle:!:]!]" tabs="js,html,css,result" styles="width: 100%; height:300px;">',
                            // 'closeWith' => '</jsf>',
                            'dropMenu' => array(
                                array(
                                    'name' => 'Javascript',
                                    'openWith' => '<code lang="javascript">',
                                    'closeWith' => '</code>',
                                ),
                                array(
                                    'name' => 'PHP',
                                    'openWith' => '<code lang="php">',
                                    'closeWith' => '</code>',
                                ),
                                array(
                                    'name' => 'SQL',
                                    'openWith' => '<code lang="sql">',
                                    'closeWith' => '</code>',
                                ),
                                array(
                                    'name' => 'HTML',
                                    'openWith' => '<code lang="html">',
                                    'closeWith' => '</code>',
                                ),
                                 array(
                                    'name' => 'JSFiddle',
                                    'openWith' => '<jsf src="[![JSFiddle]!]" tabs="js,html,css,result" styles="width: 100%; height:300px;">',
                                    'closeWith' => '</jsf>',
                                ),
                            )
                        ),
                    ),
                )

            )); ?>
		<?php echo $form->error($model,'content'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'post_status'); ?>
		<?php echo $form->dropdownlist($model,'post_status',$model->listStatus()); ?>
		<?php echo $form->error($model,'post_status'); ?>
	</div>

        <div class="row">
            <?php echo $form->labelEx($model,'tags'); ?>
            <?php echo $form->dropDownList($model, 'tags',$model->listTags(), array('multiple'=>'multiple', 'size'=>5, 'style' => 'width: 100%' )); ?>
            <?php echo $form->error($model,'tags'); ?>
        </div>



	<div class="row">
		<?php echo $form->labelEx($model,'comment_status'); ?>
		<?php echo $form->checkBox($model,'comment_status'); ?>
		<?php echo $form->error($model,'comment_status'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'is_top'); ?>
		<?php echo $form->checkBox($model,'is_top'); ?>
		<?php echo $form->error($model,'is_top'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Опубликовать' : 'Сохранить'); ?>
	</div>


<?php $this->endWidget(); ?>

</div><!-- form -->