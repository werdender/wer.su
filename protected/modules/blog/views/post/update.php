<?php
$this->breadcrumbs=array(
	//'Posts'=>array('index'),
	$model->theme => ('/post/'.$model->id.'/'.$model->theme),
	'Редактирование',
);

?>

<h1>Редактирование записи</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>