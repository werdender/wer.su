<div class="view">

	<h1>
            <?php if($data->is_top): ?>
                <span class="icon-pin-big cursor-help" title="Всегда сверху"></span>
            <?php endif; ?>

            <?php if($data->post_status == Post::STATUS_DRAFT): ?>
                <span class="icon-lock-big cursor-help" title="Черновик"></span>
            <?php endif; ?>

            <?php echo CHtml::link(CHtml::encode($data->theme), $data->url); ?>
        </h1>

        <?= Yii::app()->controller->renderPartial('blog.views.tags._index', array('data'=>$data->tags), true) ?>

        <div class="post-content">
            <?php echo $data->cutted; ?>
	</div>

        <?= Yii::app()->controller->renderPartial('blog.views.post._footer', array('data'=>$data, 'view'=>false), true) ?>

</div>