<?php
$this->breadcrumbs = array(
    //'Posts'=>array('index'),
    $model->theme,
);
?>

<h1>
    <?php if ($model->is_top): ?>
        <span class="icon-pin-big cursor-help" title="Всегда сверху"></span>
    <?php endif; ?>

    <?php if ($model->post_status == Post::STATUS_DRAFT): ?>
        <span class="icon-lock-big cursor-help" title="Черновик"></span>
    <?php endif; ?>

    <?php if ($model->post_status == Post::STATUS_ARCHIVED): ?>
        <span class="icon-denied cursor-help" title="В архиве"></span>
    <?php endif; ?>

    <?php echo $model->theme; ?>
</h1>

<?= Yii::app()->controller->renderPartial('blog.views.tags._index', array('data' => $model->tags), true) ?>

<div class="post-content">
    <?= $model ?>
</div>

<?= Yii::app()->controller->renderPartial('blog.views.post._footer', array('data' => $model, 'view' => true), true) ?>

<a name="comments"></a>
<?php
//Yii::import('blog.widgets.CommentsWidget');
if ($model->comment_status)
{
    $this->widget('DisqusWidget', array(
        'disqus_identifier' => $model->id,
        'disqus_title' => $model->theme,
        'disqus_url' => Yii::app()->params->disqus['host'] . $model->url,
    ));
}
?>

<?php if (!$model->comment_status): ?>
    <h2 class="comments-h2">комментирование отключено</h2>
<?php endif; ?>

