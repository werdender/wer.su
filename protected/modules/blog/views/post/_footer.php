<div class="post-footer">
    <div class="block gray icon-clock cursor-help" title="Время публикации"><?= $data->postDate ?></div>
    <!-- <div class="block gray icon-heart"><?= $data->postRating ?></div> -->
    <div class="block gray icon-eye cursor-help" title="Количество просмотров"><?= $data->views_count ?></div>
    <?php if (!$data->comment_status && !$view): ?>
        <div class="block right icon-comments-disabled">Комментирование отключено</div>
    <?php elseif (!$view): ?>
        <div class="block right icon-comments cursor-help" title="Количество комментариев">
            <?= CHtml::link('Комментировать', 'http://wer.su' . $data->url . '#disqus_thread') ?>
        </div>
    <?php endif; ?>
</div>

