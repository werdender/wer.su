<?php

class PostController extends Controller
{

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';
    private $_model;

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array_merge(parent::filters(), array(
            'accessControl', // perform access control for CRUD operations
                /* array(
                  'COutputCache + index',
                  'duration' => 86400,
                  'dependency' => array(
                  'class' => 'CChainedCacheDependency',
                  'dependencies' => array(
                  new CDbCacheDependency('SELECT MAX(updated) FROM {{blog_posts}}'),
                  new CExpressionDependency('Yii::app()->user->isGuest'),
                  ),
                  ),
                  'varyByParam' => array('Post_page'),
                  ),
                  array(
                  'COutputCache + view',
                  'duration' => 86400,
                  'dependency' => array(
                  'class' => 'CChainedCacheDependency',
                  'dependencies' => array(
                  new CDbCacheDependency('SELECT MAX(updated) FROM {{blog_posts}}'),
                  new CExpressionDependency('Yii::app()->user->isGuest'),
                  ),
                  ),
                  'varyByParam' => array('id'),
                  ), */
        ));
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view', 'viewTest'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update'),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     */
    public function actionView($id)
    {
        $post = $this->loadModel($id);
        if( ! isset($_GET['theme']) || Text::translitURL($post->theme) !== $_GET['theme']) {
            Yii::app()->request->redirect($post->getUrl(), true, 301);
        }
        if (!Yii::app()->session->get('view_post_' . $post->id))
        {
            $post->saveCounters(array('views_count' => 1));
            Yii::app()->session['view_post_' . $post->id] = TRUE;
        }
        $this->pageTitle = Yii::app()->name . ' - ' . $post->theme;
        $this->render('view', array(
            'model' => $post,
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $model = new Post;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Post']))
        {
            $model->attributes = $_POST['Post'];
            $model->setRelationRecords('tags', $_POST['Post']['tags']);
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Post']))
        {
            $model->attributes = $_POST['Post'];
            $model->setRelationRecords('tags', $_POST['Post']['tags']);
            //$model->tags->add() = $_POST['Post']['tags'];
            if ($model->save())
            {
                //foreach ($_POST['Post']['tags'] as $tag) {
                //}
                $this->redirect(array('view', 'id' => $model->id));
            }
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest)
        {
            // we only allow deletion via POST request
            $this->loadModel($id)->delete();

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if (!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        }
        else
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        $dataProvider = new CActiveDataProvider(Post::model()->all(), array(
                    'criteria' => array(
                        'with' => array('user', 'tags'),
                    ),
                    'pagination' => array(
                        'pageSize' => $this->module->postPerPage,
                    ),
                ));
        $this->pageTitle = Yii::app()->name . ' - Лента';
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $model = new Post('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Post']))
            $model->attributes = $_GET['Post'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id)
    {
        if ($this->_model === null)
        {
            if (isset($_GET['id']))
            {
                if (Yii::app()->user->isGuest)
                    $condition = 'post_status=' . Post::STATUS_PUBLISHED
                            . ' OR post_status=' . Post::STATUS_ARCHIVED;
                else
                    $condition = '';
                $this->_model = Post::model()->findByPk($id, $condition);
            }
            if ($this->_model === null)
                throw new CHttpException(404, 'Запрашиваемая страница не найдена.');
        }
        return $this->_model;
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'post-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
