<?php

class DisqusWidget extends CWidget {

    public $disqus_shortname;
    public $disqus_identifier;
    public $disqus_title;
    public $disqus_url;
    public $disqus_category_id;

    protected $isDeveloperMode = false;

    public function init() {
        parent::init();
        $this->disqus_shortname = Yii::app()->params->disqus['disqus_shortname'];

    }

    public function run() {
        $this->render('DisqusWidget', array(
            'disqus_shortname' => $this->disqus_shortname,
            'disqus_identifier' => $this->disqus_identifier,
            'disqus_title' => $this->disqus_title,
            'disqus_url' => $this->disqus_url,
        ));
    }

}

?>
