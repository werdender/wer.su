<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController
{
	/**
	 * @var string the default layout for the controller view. Defaults to '//layouts/column1',
	 * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
	 */
	public $layout='//layouts/column1';
	/**
	 * @var array context menu items. This property will be assigned to {@link CMenu::items}.
	 */
	public $menu=array();
	/**
	 * @var array the breadcrumbs of the current page. The value of this property will
	 * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
	 * for more details on how to specify this property.
	 */
	public $breadcrumbs=array();

        public $pageHello = '';

        public function filters() {
            return array(
                'addHeaders',
                'setHello',
            );
        }

        public function filterSetHello($filterChain) {
            $g = Yii::app()->params->siteGreetings;
            $this->pageHello = $g[rand(0, (count($g) - 1))];

            $filterChain->run();
        }

        public function filterAddHeaders($filterChain) {
            $cacheTime = 60;
            if(isset($_GET['id']) && intval($_GET['id']) > 0) {
                $command = Yii::app()->db->cache($cacheTime)->createCommand('SELECT updated FROM {{blog_posts}} WHERE id = :id')->bindParam(':id', intval($_GET['id']));
            }else{
                $command = Yii::app()->db->cache($cacheTime)->createCommand('SELECT MAX(updated) FROM {{blog_posts}}');
            }
            $dateUpdate = $command->queryScalar();

            if($dateUpdate) {
                $LastModified = gmdate("D, d M Y H:i:s \G\M\T", $dateUpdate);
                $expire = gmdate("D, d M Y H:i:s \G\M\T", time() + 600);
                header('Last-Modified: ' . $LastModified);
                header('Expires: ' . $expire);
            }
            $filterChain->run();
        }
}