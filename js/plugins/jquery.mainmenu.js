;
(function($) {
    $.fn.mainMenu = function(options) {

        var me = this,
            win = $(window),
            params = $.extend({
                fixOn: 20,
                unfixOn: 250,
                fixedCls: 'menu-fixed'
            }, options),
            isFixed = false,
            lastScrollTop = 0,
            currentScrollTop = 0,
            cUp = 0,
            cDown = 0,
            cVisible = 0;

        var showFixedMenu = function() {
            if(win.scrollTop() < params.unfixOn) return false;
            me.slideDown();
        }

        var hideFixedMenu = function() {
            me.hide();
            me.addClass(params.fixedCls);
        }

        var unfixMenu = function() {
            //isVisible = true;
            me.show();
            me.removeClass(params.fixedCls);
        }

        var onScroll = function(e) {
            currentScrollTop = win.scrollTop();
                //console.log(cst)
            //console.log(currentScrollTop, lastScrollTop, currentScrollTop < lastScrollTop)
            if(currentScrollTop > params.unfixOn || lastScrollTop < currentScrollTop) { //down
            //lasScrollTop = st;
                /*me.addClass('menu-fixed');
                me.slideDown();
                isFixed = true;*/
                cDown++;
                cVisible++;
                if(cDown > params.fixOn && ! isFixed) {
                    cDown = 0;
                    isFixed = true;
                    hideFixedMenu();
                }

                if(cVisible > 5 && isFixed) {
                    cVisible = 0;
                    me.slideUp();
                }

            }

            if(currentScrollTop < lastScrollTop && isFixed) { //up
            //lasScrollTop = st;
                /*me.addClass('menu-fixed');
                me.slideDown();
                isFixed = true;*/
                cUp++;
                cVisible = 0;
                if(cUp > params.fixOn) {
                    cUp = 0;
                    showFixedMenu();
                }
            }

            lastScrollTop = currentScrollTop;

            if(win.scrollTop() < params.unfixOn) {
                isFixed = false;
                unfixMenu();
            }
        }



        return this.each(function(options) {
            if(win.scrollTop() > params.fixOn) onScroll();
            win.scroll(onScroll);
        });
    };
})(jQuery);